package paczka;

public class Losowanie {

	public static void main(String[] args) {
		
		Wyzwiska tyrada = new Wyzwiska();
		
		int wynik1 = tyrada.tyra1(5, 7);
		int wynik2 = tyrada.tyra2(5, 3, 6);
		int wynik3 = tyrada.tyra3(4, 6, 8, 2);
		
		System.out.println(wynik1 > 10);
		System.out.println(wynik2 == 20);
		System.out.println(wynik3 < 4);
		
	}
	
}
