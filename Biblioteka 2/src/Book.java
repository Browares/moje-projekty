
public class Book {

	String tytuł;
	String autor;
	int dataWydania;
	int liczbaStron;
	String wydawca;

	Book(String bookTitle, String bookAuthor, int bookRelease, int bookPages, String bookPublisher) {
		tytuł = bookTitle;
		autor = bookAuthor;
		dataWydania = bookRelease;
		liczbaStron = bookPages;
		wydawca = bookPublisher;
	}

	void printInfo() {
		String info = tytuł + "; " + autor + "; " + dataWydania + "; " + liczbaStron + "; " + wydawca;
		System.out.println(info);
	}

}
