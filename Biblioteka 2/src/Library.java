
public class Library {

	public static void main(String[] args) {
		
		final String appName = "Biblioteka v0.3";

		Book [] books = new Book [1000];
		books[0] = new Book("Gra o Tron" , "George R.R. Martin", 1996, 345, "GMR");
		books[1] = new Book("Zatrudnijcie mnie", "Marcin Browarczyk", 2016, 100, "SDA");
		books[2] = new Book("Weźcie mnie na staż proszę!", "M.J.W. Browarczyk", 2016, 123, "Nadzieja");
		
		System.out.println(appName);
		System.out.println("Książki dostępne w bibliotece: ");
		
		books[0].printInfo();
		books[1].printInfo();
		books[2].printInfo();
		System.out.println("System może pomieści do " + books.length + " książek");
	}

}
