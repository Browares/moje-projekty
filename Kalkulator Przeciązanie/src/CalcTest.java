
public class CalcTest {

	public static void main(String[] args) {
		
		Calculator calc = new Calculator();
		
		double a = 10;
		double b = 20;
		double c = 30;
		double d = 40;
		
		System.out.println("A: " + a + "B: " + b + "C: " + c + "D: " + d );
		System.out.println("A+B=" + calc.add(a, b));
		System.out.println("A+B+C=" + calc.add(a, b, c));
		System.out.println("A+B+C+D=" + calc.add(a, b, c, d));
		

		System.out.println("A: " + a + "B: " + b + "C: " + c + "D: " + d );
		System.out.println("A-B=" + calc.substract(a, b));
		System.out.println("A-B-C=" + calc.substract(a, b, c));
		System.out.println("A-B-C-D=" + calc.substract(a, b, c, d));
		
		

	}

}
