
public class Calculator {
	
	double add (double a, double b) {
		return a + b;
    }
	
	double add (double a, double b, double c) {
		return  a + b + c;
	}
	
	double add (double a, double b, double c, double d) {
		return a + b + c + d;
	}
	
	double substract (double a, double b) {
		return a - b;
	}
	
	double substract (double a, double b, double c) {
		return a - b - c;
	}
	
	double substract (double a, double b, double c, double d) {
		return a - b - c - d;
	}
}
