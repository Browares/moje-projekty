package paczka;

import java.util.Random;

public class Zadanie {

	public static void main(String[] args) {
		
		int x = new Random().nextInt(100);
		int y = new Random().nextInt(100);
		
		System.out.println("Czy x jest większy od y?");
		System.out.println(x > y);
		System.out.println("Czy x pomnożone przez 2 jest większe od y?");
		System.out.println(x * 2 > y);
		System.out.println("Czy y jest mniejsze od sumy x+3 i jednocześnie większe od x pomniejszonego o 2?");
		System.out.println((y < x + 3) && (y > x - 2));
		
		
		double iloczyn = x * y;
		
		System.out.println("Czy iloczyn liczb x i y jest parzysty? (Wykorzystaj do sprawdzenia operację modulo)");
		if(iloczyn%2 == 0) {
			System.out.println("Parzysta");
		}
		
		else {
			System.out.println("Nieparzysta");
		}
		
		boolean wynik = ((x * y) % 2 == 0);
		
		System.out.println("Iloczyn parzysty true czy false?");
		System.out.println(wynik);
		

	}

}
