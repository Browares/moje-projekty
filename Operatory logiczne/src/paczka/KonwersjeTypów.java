package paczka;

public class KonwersjeTypów {

	public static void main(String[] args) {
		
		int number1 = 5;
		double number2 = 10.456;
		
		double zamiana = (double)number1;
		int zamiana2 = (int)number2;
		
		System.out.println("Zamiana z int na double: " + zamiana);
		System.out.println("Zamiana2 z double na int " + zamiana2);

	}

}
