package paczka;

import java.util.Scanner;

public class SumNumbers {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Wpisz ile liczb chcesz zsumować: ");
		int numbers = sc.nextInt();
		
		int sum = 0;
		while (numbers-- > 0) {   //dekrementacja "--"
			System.out.println("Wpisz kolejną liczbę: ");
			sum = sum + sc.nextInt();
		}
		
		System.out.println("Suma wszystkich podanych: " + sum);
		
		sc.close();

	}

}
