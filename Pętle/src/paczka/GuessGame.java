package paczka;

import java.util.Scanner;

public class GuessGame {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		
		int number = 5;
		int userInput;
		System.out.println("Zgadnij liczbę od 1 do 10!");
		
		while((userInput = reader.nextInt()) != number) {
			if(userInput > number) {
				System.out.println("Podana liczba jest za duża:(");
			}else {
				System.out.println("Podana liczba jest za mała:(");
			}
		}
		
		System.out.println("Brawo! Podałeś właściwą liczbę:)");

		reader.close();
	}

}
