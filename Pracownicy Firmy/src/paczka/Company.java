package paczka;

public class Company {

	public static void main(String[] args) {
Employee pracownik1 =  new Employee();
        
        pracownik1.name = "Marcin";
        pracownik1.surname = "Browarczyk";
        pracownik1.DOB = "12.10.1989";
        pracownik1.workTime = 3;
        		
	
	    Employee pracownik2 = new Employee();
	    
	    pracownik2.name = "Agata";
	    pracownik2.surname = "Kołba";
	    pracownik2.DOB = "27.02.1991";
	    pracownik2.workTime = 2;
	  
	    
	    Employee pracownik3 = new Employee();
	    
	    pracownik3.name = "Stefka";
	    pracownik3.surname = "Bąkilda";
	    pracownik3.DOB = "06.01.2015";
	    pracownik3.workTime = 1;
	    
	    String pracownik1Info = pracownik1.name + " " + pracownik1.surname + " Data urodzenia: "
	                          + pracownik1.DOB + " Staż pracy: " + pracownik1.workTime;
	    
	    System.out.println(pracownik1Info);
	    
	    String pracownik2Info = pracownik2.name + " " + pracownik2.surname + " Data urodzenia: "
	    		              + pracownik2.DOB + " Staż pracy: " + pracownik2.workTime;
	    
	    System.out.println(pracownik2Info);
	    
	    String pracownik3Info = pracownik3.name + " " + pracownik3.surname + " Data urodzenia: "
	              + pracownik3.DOB + " Staż pracy: " + pracownik3.workTime;
	    
	    System.out.println(pracownik3Info);

	}

}
