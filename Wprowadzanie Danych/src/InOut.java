import java.util.Scanner;

public class InOut {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		String firstName;
		String lastName;
		int age;
		
		System.out.println("Wprowadź swoje imię: ");
		firstName = input.nextLine();
		System.out.println("Wprowadź swoje nazwisko: ");
		lastName = input.nextLine();
		System.out.println("Wprowadź swój wiek: ");
		age = input.nextInt();
		
		input.close();
		
		System.out.println("Cześć " + firstName + " " + lastName);
		System.out.println("Masz już: " + age + ", starość nie radość:(");
		

	}

}
