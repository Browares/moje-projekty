package paczka;

import java.util.Scanner;

public class CalcInput {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		
		System.out.println("Liczba A: ");
		double a = input.nextDouble();
		input.nextLine();
		System.out.println("Operator: ");
		String operator = input.nextLine();
		System.out.println("Liczba B: ");
		double b = input.nextDouble();
		
		Calculator calc = new Calculator();
		double result = calc.calculate(a, b, operator);
		System.out.println(a + operator + b + " = " + result );
		
		input.close();

	}

}
