
public class TabCalc {

	public static void main(String[] args) {
		
		double [][] tab = new double [3] [3];
		
		tab [0][0] = 1.0;
		tab [0][1] = 1.5;
		tab [0][2] = 2.0;
		tab [1][0] = 1.5;
		tab [1][1] = 2.0;
		tab [1][2] = 2.5;
		tab [2][0] = 2.0;
		tab [2][1] = 2.5;
		tab [2][2] = 3.0;
		
		double result = (tab[0][0]*tab[1][1]*tab[2][2]) + (tab[0][2]*tab[1][1]*tab[2][0]);
        System.out.println("Suma iloczynów przekątnych tablicy: " + result);
        
        result = (tab[0][1]+tab[1][1]+tab[2][1]) * (tab[1][0]+tab[1][1]+tab[1][2]);
        System.out.println("Iloczyn sum środkowego wiersza i środkowej kolumny tablicy: " + result);
        
        result = tab[0][0] + tab[0][1] + tab[0][2] + tab[1][0] + tab[1][2] + tab[2][0] + tab[2][1] + tab[2][2];
        System.out.println("Sumę wszystkich elementów znajdujących się przy krawędzi tablicy: " + result);
		
		

	}

}
