package paczka;

public class Calculate {

	public static void main(String[] args) {
		
		Calculator calc = new Calculator();
		
		double addResult = calc.add(2.0, 4.5);
		System.out.println(addResult);
		double subResult = calc.substract(5.0, 2.0);
		System.out.println(subResult);
		double mulResult = calc.multiply(3.0, 4.0);
		System.out.println(mulResult);
		double divResult = calc.divide(10.0, 3.0);
		System.out.println(divResult);
		
	}

}
