package paczka;

public class UserManager {

	public static void main(String[] args) {
		User user = new User ("Marcin", "Browarczyk");
		
		System.out.println("Wybierz opcję: ");
		System.out.println("0 - Wyjście z programu");
		System.out.println("1 - Wyświetl informacje o użytkowniku");
		System.out.println("2 - Edytuj dane użytkownika");
		
		int option = 1;
		
		if(option==0) {
			System.out.println("Bye bye!");		
		} else if(option==1) {
			System.out.println("Użytkownik: " + user.getFirstName() + " " + user.getLastName());
		}else if(option==2) {
			user.setFirstName("Martin");
			user.setLastName("Beerman");
			System.out.println("Zmieniono dane na: " + user.getFirstName() + " " + user.getLastName());
		}

	}

}
